package cest.mypet.cadastro.loc.UF;

public class UF {
	
  private int cod;
  private String descricao;
   
  
  public void setCodUF(int cod) {
	  
	  this.cod = cod;
	  
  }
  
  public int GetCodUF() {
	  
	  return this.cod;
  }
  
  public void setDesUF(String descricao) {
	  
	  this.descricao = descricao;
  }
	
  public String GetDesUF() {
	  return this.descricao;
  }

}
