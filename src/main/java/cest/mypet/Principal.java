package cest.mypet;
import cest.mypet.cadastro.Animal.Animal;
import cest.mypet.cadastro.Pessoa.Pessoa;
import cest.mypet.cadastro.TipoAnimal.TipoAnimal;
import cest.mypet.cadastro.loc.Cidade.Cidade;
import cest.mypet.cadastro.loc.UF.UF;




public class Principal {

	private static final TipoAnimal Malhoso = null;

	public static void main(String[] args) {
	
		// Classe do Animal
		
		System.out.println("\n\t\t Codigo do Animal , Seu Nome & Sua Idade \n");
		// Nome do Animal
		Animal cachorro = new Animal();
		cachorro.setNome("Lula");
		System.out.println("Nome Do Cachorro: " + cachorro.GetNome());
		
	
		// Idade do Animal
		
		Animal idade1 = new Animal();
		idade1.setIdade(10);
		System.out.println("Idade do Animal: " + idade1.GetIdade());
		
	
		
		//   Classe do Tipo Animal
		
		System.out.println("\n\t\t Cod do Tipo Animal e Sua Descrição \n");
		// Codigo do Tipod do Animal
		TipoAnimal cdgo = new TipoAnimal();
		cdgo.setCod(1);
		System.out.println("Codigo do Tipo do Animal: " + cdgo.GetCod());

		// Descrição do Tipo Animal
		TipoAnimal desc = new TipoAnimal();
		desc.setDesc("O Animal é Magro...");
		System.out.println("Descrição do Animal: " + desc.GetDesc());
		
	
		
		// Classe da Pessoa
		// Nome da Pessoas
		System.out.println("\n\t\t Nomes das Pessoas \n");
		
		Pessoa pes1 = new Pessoa();
		pes1.setNomePessoa("Mateus");
		Pessoa pes2 = new Pessoa();
		pes2.setNomePessoa("Thiago");
		
		System.out.println("Nome da Pessoa(1): " + pes1.GetNomePessoa());
		System.out.println("Nome da Pessoa(2): " + pes2.GetNomePessoa());
		
		
		// Nome da Cidades
		
		System.out.println("\n\t\t Nome da Cidades  \n");
		Pessoa cid1 = new Pessoa();
		cid1.setNomeCidade("Los Angels");
		Pessoa cid2 = new Pessoa();
		cid2.setNomeCidade("Chicago");
		System.out.println("Nome da Cidade(1): " + cid1.GetNomeCidade());
		System.out.println("Nome da Cidade(2): " + cid2.GetNomeCidade());
		
		
		// Classe UF
		// Codigo de Acesso UF
		
		System.out.println("\n\t\t Descrição da Classe UF é o Codigo \n");
		UF ufcod1 = new UF();
		ufcod1.setCodUF(1);
		System.out.println("Codigo do UF: " + ufcod1.GetCodUF());
		
		// Descrição do UF
		
		
		UF desUF = new UF();
		desUF.setDesUF("É um Estado do MARANHÃO (MA) ");
		System.out.println("Descrição do UF: " + desUF.GetDesUF());
		System.out.println("\n\n");
		
		
		// Classe da Cidade
		// Nome do UF Da Cidade
		
		System.out.println("\n\t\t Nome do UF Da Cidade \n");
		Cidade cidUF = new Cidade();
		cidUF.setCidadeUF("MA");
		System.out.println("Nome do UF da Cidade: " + cidUF.GetCidadeUF() );
		
	}

}
